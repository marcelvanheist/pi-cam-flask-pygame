import pygame
from pygame.locals import *


class Gfx:
  def __init__(self, folder, name, ext):
    self.name = name
    try:
      self.bitmap = pygame.image.load(folder + '/' + name + '.' + ext)
    except:
      pass

class Button:
  def __init__(self, rect, **kwargs):
    self.rect     = rect # Bounds
    self.color    = None # Background fill color, if any
    self.iconBg   = None # Background Icon (atop color fill)
    self.iconFg   = None # Foreground Icon (atop background)
    self.bg       = None # Background Icon name
    self.fg       = None # Foreground Icon name
    self.callback = None # Callback function
    self.value    = None # Value passed to callback
    for key, value in kwargs.iteritems():
      if   key == 'color': self.color    = value
      elif key == 'bg'   : self.bg       = value
      elif key == 'fg'   : self.fg       = value
      elif key == 'cb'   : self.callback = value
      elif key == 'value': self.value    = value

  def selected(self, screen, event_type, pos):
    x1 = self.rect[0]
    y1 = self.rect[1]
    x2 = x1 + self.rect[2] - 1
    y2 = y1 + self.rect[3] - 1
    if ((pos[0] >= x1) and (pos[0] <= x2) and
        (pos[1] >= y1) and (pos[1] <= y2)):
      if event_type == MOUSEBUTTONDOWN:
        pass
        # pos = False
        # size = False
        # print self.iconFg, self.iconBg
        # if self.iconFg:
        #   pos = (self.rect[0]+(self.rect[2]-self.iconFg.bitmap.get_width())/2,
        #           self.rect[1]+(self.rect[3]-self.iconFg.bitmap.get_height())/2)
        #   size = (self.iconFg.bitmap.get_width(), self.iconFg.bitmap.get_height())
        # elif self.iconBg:
        #   pos = (self.rect[0]+(self.rect[2]-self.iconBg.bitmap.get_width())/2,
        #           self.rect[1]+(self.rect[3]-self.iconBg.bitmap.get_height())/2)
        #   size = (self.iconBg.bitmap.get_width(), self.iconBg.bitmap.get_height())
        # else:
        #   pos = (self.rect[0], self.rect[1])
        #   size = (self.rect[2], self.rect[3])
        # active = pygame.Surface(size)
        # active.set_alpha(25)
        # active.fill((117, 0, 0))
        # screen.blit(active, pos)
        # pygame.display.update()
        # return True
      elif event_type == MOUSEBUTTONUP:
        if self.callback:
          if self.value is None: self.callback()
          else:                  self.callback(self.value)
        return True
    return False

  def draw(self, screen):
    # touch_area = pygame.Surface((self.rect[2], self.rect[3]))
    # touch_area.set_alpha(30)
    # touch_area.fill((0,0,255))
    # screen.blit(touch_area, (self.rect[0],self.rect[1]))
    if self.color:
      screen.fill(self.color, self.rect)
    if self.iconBg:
      screen.blit(self.iconBg.bitmap,
        (self.rect[0]+(self.rect[2]-self.iconBg.bitmap.get_width())/2,
         self.rect[1]+(self.rect[3]-self.iconBg.bitmap.get_height())/2))
    if self.iconFg:
      screen.blit(self.iconFg.bitmap,
        (self.rect[0]+(self.rect[2]-self.iconFg.bitmap.get_width())/2,
         self.rect[1]+(self.rect[3]-self.iconFg.bitmap.get_height())/2))

  def setBg(self, icons, name):
    if name is None:
      self.iconBg = None
    else:
      for i in icons:
        if name == i.name:
          self.iconBg = i
          break