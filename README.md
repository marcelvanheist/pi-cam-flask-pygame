# SETUP PI-CAM

## Create micro SD card

- Download Raspbian Jessie Lite [here](https://www.raspberrypi.org/downloads/raspbian/)
- Flash image using 'Etcher'
- Add file `ssh` (no extension) to /boot on SD card
- Insert micro SD card
- login with pi/raspberry

## Setup WIFI and SSH

- enter `sudo nano /etc/wpa_supplicant/wpa_supplicant.conf`
- add to bottom of file and save file:

```
network={
    ssid="Your_SSID"
    psk="Your_wifi_password"
}
```

- force connecting to wifi: `sudo ifup --force wlan0`
- get IP by typing: `ifconfig wlan0 | grep "inet addr:"`

### Enable SSH & Camera

- enter `sudo raspi-config`
  - Interfacing Options -> SSH
  - Interfacing Options -> Camera
  - Set localization, time zone (and system locale)
  - Expand root partition
  - Set boot to console and autologin
  - Disable wait for network on boot

### Use Terminal on Mac

- ssh pi@IP_ADDRESS


## Setup Raspbian

- `sudo apt-get update`

### Install dependencies

#### Update firmware

- `sudo apt-get install rpi-update`
- `sudo rpi-update`
- `sudo reboot`

#### Install Pypthon / Flask

- `sudo apt-get install python-pip`
- `sudo pip install Flask`
- `sudo apt-get install python-picamera`
- `sudo apt-get install python-pygame`

## Touch Screen

### Easy Install

- `curl -SLs https://apt.adafruit.com/add-pin | sudo bash`
- `sudo apt-get install raspberrypi-bootloader adafruit-pitft-helper raspberrypi-kernel`
- `sudo adafruit-pitft-helper -t 28c`

### Calibration

- `sudo nano /etc/pointercal`
- `sudo nano installsdl.sh`

```
#!/bin/bash
  
#enable wheezy package sources
echo "deb http://archive.raspbian.org/raspbian wheezy main
" > /etc/apt/sources.list.d/wheezy.list

#set stable as default package source (currently jessie)
echo "APT::Default-release \"stable\";
" > /etc/apt/apt.conf.d/10defaultRelease

#set the priority for libsdl from wheezy higher then the jessie package
echo "Package: libsdl1.2debian
Pin: release n=jessie
Pin-Priority: -10
Package: libsdl1.2debian
Pin: release n=wheezy
Pin-Priority: 900
" > /etc/apt/preferences.d/libsdl

#install
apt-get update
apt-get -y --force-yes install libsdl1.2debian/wheezy
```

- `sudo chmod +x installsdl.sh`
- `sudo ./installsdl.sh`

### Power button pin

- `sudo nano /etc/modprobe.d/adafruit.conf`
- change 23 to 17

### Screen speed

- `sudo nano /boot/config.txt`
- change to: `dtoverlay=pitft28c,rotate=90,speed=80000000,fps=50`

## pi-cam

- `sudo apt-get install git`
- `git clone https://gitlab.com/marcelvanheist/pi-cam-flask-pygame.git`
- `sudo pip install flask-cors`

### Pillow
- `sudo apt-get install python-dev python-setuptools`
- `sudo apt-get install libjpeg62 zlib1g-dev libfreetype6-dev liblcms1-dev libjpeg-dev`
- `sudo pip install Pillow`

### Color correct
- `sudo apt-get install mercurial`
- `hg clone https://bitbucket.org/aihara/colorcorrect`
- `cd colorcorrect`
- `sudo python setup.py build`
- `sudo python setup.py install`

### Create empty folders
- `cd pi-cam-flask-pygame`
- `mkdir thumbs`
- `mkdir static`

### Create wifi.txt
- `sudo nano /boot/wifi.txt`

```
ssid=example_ssid
psk=example_psk
```

## Supervisor

- `sudo apt-get install supervisor`
- `sudo service supervisor restart`
- `sudo update-rc.d supervisor defaults`
- `sudo systemctl enable supervisor`
- `sudo systemctl start supervisor`
- `sudo nano /etc/supervisor/conf.d/multicam.conf`

```
[program:multicam]
command=sudo /usr/bin/python /home/pi/pi-cam-flask-pygame/multicam.py
autostart=true
autorestart=true
stderr_logfile=/home/pi/pi-cam-flask-pygame/multicam_err.log
stdout_logfile=/home/pi/pi-cam-flask-pygame/multicam_out.log
stopsignal=QUIT
killasgroup=true
```

- `sudo supervisorctl reread`
- `sudo supervisorctl update`

# Reboot

- `sudo reboot`