import os
import re
import io
import sys
import time
import atexit
import fnmatch
import random
import subprocess
import signal
import threading
import urllib2
import socket

import logging
from logging.handlers import RotatingFileHandler

import RPi.GPIO as gp
import pygame
from pygame.locals import *
import picamera
from PIL import Image
from flask import Flask, Response, request, jsonify
from flask_cors import CORS, cross_origin

import colorcorrect.algorithm as cca
from colorcorrect.util import from_pil, to_pil

import ui

# DEBUG / DEV VARIABLES
CHANGE_WIFI = True

# FLASK CONSTANTS
F_PORT = 80
app = Flask(__name__)
CORS(app)

# CONSTANTS
SCREEN_TIMEOUT = 60 #Seconds
SCREEN_WIDTH = 320
SCREEN_HEIGHT = 240
WORDS = ["dacroces","cludion","unistoinsubtle","consionater","memercum","rivierenhal","turess","fortedgened","cheenhows","quisitiestiary","saddendents","elesce","jumpors","undeuticallow","ioness","chopia","statted","ackers","pearis","purifted","footted","seectiger","underapping","brewifie","excessianiar","unarmles","grattimanson","lianyings","sclock","funnipattran","craffluck","fraugmy","hospornielling","wounicks","simputale","eastatiom","aweens","sandess","leenricandum","sagges","suramistle","destructriates","sapson","gonzibers","piresta","shamins","appersethron","japackoon","tremateds","rononsequitor"]
PAGE_FOLDER = '/home/pi/pi-cam-flask-pygame/page/'
ICON_FOLDER = '/home/pi/pi-cam-flask-pygame/icon/'
GFX_FOLDER = '/home/pi/pi-cam-flask-pygame/gfx/'
PICTURE_FOLDER = '/home/pi/pi-cam-flask-pygame/static'
THUMB_FOLDER = '/home/pi/pi-cam-flask-pygame/thumbs'
FAST_LOOP = (1/50)
SLOW_LOOP = (3/4)
FILE = '/home/pi/pi-cam-flask-pygame/main_log.log'
MAX_BYTES = 10000
BACKUP_COUNT = 5
FORMAT = '%(asctime)-6s: %(name)s - %(levelname)s - %(message)s'

# GLOBALS
font = None
backlight = True
screen = None
camera = None
logger = None
last_frame = False
error = "Unknown error"
icons = []
pages = []
gfxs = []
reviews = []
loop_time = FAST_LOOP
btn_cam_select = None
btn_cam_review = None
buttons = []
run_main = False
pygame_running = False

# CAMERA VARIABLES
rgb = bytearray(SCREEN_WIDTH * SCREEN_HEIGHT * 3)
DEFAULT_PREVIEW_SIZE = (320, 240)
WIDE_PREVIEW_SIZE = (320, 180)
preview_size = DEFAULT_PREVIEW_SIZE
INITIAL_PREVIEW_FRAMERATE = 25
SLOW_PREVIEW_FRAMERATE = 25
picture_size = (1296, 972)
swap_delay = 0.025
CAMERA_TIMEOUT = 2 #seconds
PICTURE_FRAMERATE = 25
VFLIP = False
HFLIP = False
camera_flips = [False, True, True, False] # camera_flips = [False, False, False, False]
JPEG_QUALITY = 60
cam_correction = -2
frames = 8
stream_list = None
capture_times = []
use_video_port = True
fps = 0

# https://pypi.python.org/pypi/colorcorrect
default_image_correction = 1
image_correction = default_image_correction # 0 # 0 or 7, 7 is slower
# 0 = OFF
# 1 = grey_world // pretty good <-
# 2 = max_white  // NOT DOING MUCH
# 3 = stretch // less good then 0, not good
# 4 = retinex // better then 2, but not good enough
# 5 = retinex_with_adjust // still bit red difference, not good enough
# 6 = standard_deviation_weighted_grey_world // pretty good slight difference still
# 7 = standard_deviation_and_luminance_weighted_gray_world // makes non red's a bit too red
# 8 = luminance_weighted_gray_world // REALLY GOOD <-

# CAMERA SETTINGS
iso = 0
iso_range = [0, 100, 200, 300, 400, 500, 600, 700, 800]
shutter = 0
shutter_us = 0
shutter_range = [0, 30, 60, 125, 250, 500, 1000, 2000]
exposure_compensation = 0
exposure_compensation_range = [-24, -20, -15, -10, -5, 0, 5, 10, 15, 20, 24]
quality = 0
quality_range = [0,1]
size = 0
size_range = [-1,0,1,2]
size_values = [(640, 480), (1296, 972), (1920, 1080), (2592, 1944)]
color = 0
color_range = [0,  # Filter, manual wb
               1,  # No filter, manual wb
               2,  # Filter, auto wb
               3]  # No filter, auto wb
color_meanings = ['Manual white balance, grey_world color-filter',
                  'Manual white balance, no color-filter',
                  'Auto white balance, grey-world color-filter',
                  'Auto white balance, no color-filter']
brightness = 50
brightness_range = [0, 10, 20, 30, 35, 40, 45, 50, 55, 60, 65, 70, 80, 90, 100]
contrast = 0
contrast_range = [-100, -90, -80, -70, -60, -50, -40, -30, -20, -10, 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
preview_framerate = INITIAL_PREVIEW_FRAMERATE

# WIFI VARIABLES
ssid = False
psk = False
boot_ssid = False
boot_psk = False
DEFAULT_BOOT_SSID = "example_ssid"
DEFAULT_BOOT_PSK = "example_psk"

# GPIO VARIABLES
current_camera = 0
GPIO_MODE = gp.BOARD
CAMERA_NR_OFFSET = -2
PINS = [33, 37, 36]
PIN_SETTNGS = [[False, False, True],
               [True, False, True],
               [True, True, False],
               [False, True, False]]
BACKLIGHT_PIN = 12
POWER_BUTTON = 11
SHUTTER_BUTTON = 13

gpio_powerbutton_value = 1
gpio_shutterbutton_value = False
allow_shutterbutton = False

# STATE VARIABLES
page = 7
prev_page = -1
update_screen = True
loading = False
preview = False
review = False
last_preview = False
is_capturing = False
gfx_blit = False
review_camera = current_camera
loader_add_c = False
loader_position = False

# SERVER METHODS
@app.route("/get/new_photos")
def get_new_photos():
  global page
  response = {
    "new_photos": False
  }
  if review and len(reviews) > 0:
    url = "http://" + socket.gethostname() + ".local/static/"
    photos = [fn for fn in os.listdir(PICTURE_FOLDER) if fn.lower().endswith(('.jpg', 'jpeg'))]
    photos.sort()
    sizes = [os.path.getsize(PICTURE_FOLDER+"/"+photo) for photo in photos]
    photos = [url + photo for photo in photos]
    logger.debug(photos)
    response["new_photos"] = photos
    sub_capture_times = capture_times[4:] if frames == 8 else capture_times
    response["photo_info"] = {
      "fps": fps,
      "capture_times": sub_capture_times,
      "iso": iso,
      "shutter": shutter,
      "exposure_compensation": exposure_compensation,
      "quality": quality,
      "size": size_values[size_range.index(size)],
      "brightness": brightness,
      "contrast": contrast,
      "sizes": sizes,
      "color": {
        "value": color,
        "description": color_meanings[color],
        "image_correction": image_correction
      }
    }
    # page = 19
  else:
    logger.debug(reviews)
    logger.debug(review)
    logger.debug("no reviews")
  return jsonify(response)


@app.route("/unlock")
def unlock():
  response = {"message":False}
  if page == 19:
    back_from_preview()
    response = {"message":True}
  if not backlight:
    open_screen()
  return jsonify(response)


@app.route("/lock")
def lock():
  global page
  page = 19
  return jsonify({"message":True})

# SYSTEM METHODS
def reboot():
  time.sleep(2)
  close_screen()
  os.system("sudo reboot")
def shutdown():
  time.sleep(2)
  close_screen()
  gp.setup(POWER_BUTTON, gp.OUT)
  gp.output(POWER_BUTTON, True)
def close_screen():
  global backlight, preview, review
  preview = False
  gp.output(BACKLIGHT_PIN, False)
  backlight = False
  logger.debug("close screen complete")
def open_screen():
  global backlight, preview, loop_time
  preview = last_preview
  gp.output(BACKLIGHT_PIN, True)
  backlight = True
  signal.alarm(SCREEN_TIMEOUT)
  loop_time = FAST_LOOP

def timeout_handler(signal, frame):
  global last_preview, loop_time
  last_preview = preview
  loop_time = SLOW_LOOP
  close_screen()
def setup_logger():
  global logger
  logger = logging.getLogger()
  hdlr = RotatingFileHandler(FILE, maxBytes=MAX_BYTES, backupCount=BACKUP_COUNT)
  hdlr.setFormatter(logging.Formatter(FORMAT))
  logger.addHandler(hdlr)
  logger.setLevel(logging.DEBUG)
  logger.info("Logger initiated")

# GPIO METHODS
def select_camera(camara_number):
  global current_camera
  for i, value in enumerate(PIN_SETTNGS[camara_number]):
    gp.output(PINS[i], value)
    while True:
      if gp.input(PINS[i]) == (1 if value else 0):
        break
  current_camera = camara_number
def setup_gpio():
  global gpio_powerbutton_value, gpio_shutterbutton_value
  gp.setwarnings(False)
  gp.setmode(GPIO_MODE)
  gp.setup(BACKLIGHT_PIN, gp.OUT)
  gp.output(BACKLIGHT_PIN, True)
  for pin in PINS:
    gp.setup(pin, gp.OUT)
  select_camera(current_camera)

  gp.setup(POWER_BUTTON, gp.IN, pull_up_down=gp.PUD_UP)
  gpio_powerbutton_value = gp.input(POWER_BUTTON)

  gp.setup(SHUTTER_BUTTON, gp.IN, pull_up_down=gp.PUD_UP)
  gpio_shutterbutton_value = gp.input(SHUTTER_BUTTON)

  logger.debug(gpio_shutterbutton_value)

def switch_cameras():
  global current_camera, update_screen
  current_camera += 1
  if current_camera > 3:
    current_camera = 0
  select_camera(current_camera)
  btn_cam_select.setBg(icons, 'switcher%d' % current_camera)
  update_screen = True

# REVIEW METHODS
def switch_review():
  global review_camera, update_screen
  review_camera += 1
  if review_camera > 3:
    review_camera = 0
  btn_cam_review.setBg(icons, 'switcher%d' % review_camera)
  update_screen = True
def load_reviews():
  global reviews
  for timeout in [0.5, 0.5, 1, 1]:
    reviews = []
    thumbnails = os.listdir(THUMB_FOLDER)
    thumbnails.sort()
    for filename in thumbnails:
      if filename.endswith(('_thumb.jpg')):
        reviews.append(pygame.image.load(THUMB_FOLDER + '/' + filename))
    if len(reviews) == 4:
      return True
    logger.debug("didn't find 4 reviews, going to sleep %d seconds" % timeout)
    time.sleep(timeout)
  return False 
# WIFI METHODS
def setup_wifi():
  if CHANGE_WIFI:
    conf_file = open('/etc/wpa_supplicant/wpa_supplicant.conf','r+')
    new_conf = ""
    for line in conf_file.readlines():
      line = re.sub(r'ssid=.+',r'ssid="'+ssid+r'"', line)
      line = re.sub(r'psk=.+',r'psk="'+psk+r'"', line)
      new_conf += line
    conf_file.seek(0)
    conf_file.write(new_conf)
    conf_file.truncate()
    conf_file.close()

def gen_ssid_psk():
  global ssid, psk
  ssid = random.choice(WORDS)
  psk = random.choice(WORDS)
  while len(psk) < 8 or psk == ssid:
    psk = random.choice(WORDS)

def reload_wifi_conf():
  gen_ssid_psk()
  load_page(0)

def show_wifi_conf():
  global ssid, psk

  if not ssid:
    gen_ssid_psk()

  text = font.render(ssid, True, (255, 255, 255))
  text_rect = text.get_rect()
  screen.blit(text, (SCREEN_WIDTH / 2 - text_rect.width / 2, 99))
  
  text = font.render(psk, True, (255, 255, 255))
  text_rect = text.get_rect()
  screen.blit(text, (SCREEN_WIDTH / 2 - text_rect.width / 2, 154))

def empty_boot_wifi():
  if ssid != boot_ssid or psk != boot_psk:
    preset_file = open('/boot/wifi.txt','w') 
    preset_file.write('ssid=%s\n' % DEFAULT_BOOT_SSID)
    preset_file.write('psk=%s' % DEFAULT_BOOT_PSK)
    preset_file.close()

def get_current_ssid():
  global ssid, psk, boot_ssid, boot_psk
  try:
    preset_file = open('/boot/wifi.txt','r')
    for line in preset_file.readlines():
      if line.find("ssid") >= 0:
        b_ssid = line.strip().split('=')[1]
        if b_ssid != DEFAULT_BOOT_SSID:
          logger.debug("Found new SSID in wifi.txt: %s" % b_ssid)
          boot_ssid = b_ssid
      if line.find("psk") >= 0:
        b_psk = line.strip().split('=')[1]
        if b_psk != DEFAULT_BOOT_PSK:
          boot_psk = b_psk
  except Exception as e:
    logger.error("ERROR 6")
    logger.debug(e)

  conf_file = open('/etc/wpa_supplicant/wpa_supplicant.conf','r')
  for line in conf_file.readlines():
    if line.find("ssid") >= 0:
      ssid = line.strip().split('=')[1][1:-1]
    if line.find("psk") >= 0:
      psk = line.strip().split('=')[1][1:-1]
  
  if ssid != boot_ssid or psk != boot_psk:
    if boot_psk and boot_psk:
      ssid = boot_ssid
      psk = boot_psk
      logger.debug("found new ssid and psk in /boot/wifi.txt ssid: %s, psk: %s" % (boot_ssid, boot_psk))
      setup_wifi()
      confirm_wifi(False)

def confirm_wifi(gotopage = True):
  global page
  if CHANGE_WIFI:
    subprocess.Popen(['sudo', 'ifup', '--force', 'wlan0'])
  if gotopage:
    page = 1
def internet_on():
  timeout_check_start = time.time()
  for timeout in [1,1,2,2,3,4,5,10,15]:
    try:
      response=urllib2.urlopen('http://www.google.com',timeout=timeout)
      if timeout == 1:
        time.sleep(0.5)
      return True
    except urllib2.URLError as e:
      logger.error('connection error after timeout %d' % timeout)
      logger.error("ERROR 1")
      logger.error(e)
      pass
    timeout_check_end = time.time()
    if timeout_check_end - timeout_check_start < timeout:
      logger.debug('failed too fast (%d), manual timeout of %d' % (timeout_check_end - timeout_check_start, timeout))
      time.sleep(timeout)
  return False
# TOUCH SCREEN SETUP
def setup_touch_screen():
  os.putenv('SDL_VIDEODRIVER', 'fbcon')
  os.putenv('SDL_FBDEV', '/dev/fb1')
  os.putenv('SDL_MOUSEDEV', '/dev/input/touchscreen')
  os.putenv('SDL_MOUSEDRV', 'TSLIB')

# CAMERA METHODS
def delete_all_jpeg():
  for filename in os.listdir(THUMB_FOLDER):
    if filename.endswith(('.jpg', '.jpeg')):
      os.unlink(THUMB_FOLDER + "/" + filename)
  for filename in os.listdir(PICTURE_FOLDER):
    if filename.endswith(('.jpg', '.jpeg')):
      os.unlink(PICTURE_FOLDER + "/" + filename)
def setup_camera():
  global camera, preview
  try:
    camera = picamera.PiCamera()
    picamera.PiCamera.CAPTURE_TIMEOUT = CAMERA_TIMEOUT
    atexit.register(camera.close)
    camera.resolution = preview_size
    camera.framerate = preview_framerate
    camera.video_denoise = True
    camera.vflip = VFLIP
    camera.hflip = HFLIP
  except Exception as e:
    error = "Camera error, check connections"
    preview = False
    page = 5
    logger.error("ERROR 2")
    logger.debug(e)

def image_stream_generator():
  for f, stream in enumerate(stream_list):
    time.sleep(swap_delay)   # SD Card Bandwidth Correction Delay
    select_camera((f+cam_correction) % 4)
    time.sleep(swap_delay)   # SD Card Bandwidth Correction Delay
    capture_times.append(int(time.time() * 1000))
    yield stream

def process_photos_stream():
  global loading, page, error, preview
  if frames == 8:
    partial_stream_list = stream_list[4:]
    partial_capture_times = capture_times[4:]
  else:
    partial_stream_list = stream_list
    partial_capture_times = capture_times
  
  logger.debug("image_correction = %d" % image_correction)

  for frame, stream in enumerate(partial_stream_list):
    try:
      image = Image.open(stream)
      if image_correction == 1:
        logger.info("Using color correction: grey_world")
        image = to_pil(cca.grey_world(from_pil(image)))
      elif image_correction == 2:
        logger.info("Using color correction: max_white")
        image = to_pil(cca.max_white(from_pil(image)))
      elif image_correction == 3:
        logger.info("Using color correction: stretch")
        image = to_pil(cca.stretch(from_pil(image)))
      elif image_correction == 4:
        logger.info("Using color correction: retinex")
        image = to_pil(cca.retinex(from_pil(image)))
      elif image_correction == 5:
        logger.info("Using color correction: retinex_with_adjust")
        image = to_pil(cca.retinex_with_adjust(from_pil(image)))
      elif image_correction == 6:
        logger.info("Using color correction: standard_deviation_weighted_grey_world")
        image = to_pil(cca.standard_deviation_weighted_grey_world(from_pil(image))) #,subblock width(default:20), subblock height(default:20)
      elif image_correction == 7:
        logger.info("Using color correction: standard_deviation_and_luminance_weighted_gray_world")
        image = to_pil(cca.standard_deviation_and_luminance_weighted_gray_world(from_pil(image))) #,subblock width(default:20), subblock height(default:20)
      elif image_correction == 8:
        logger.info("Using color correction: luminance_weighted_gray_world")
        image = to_pil(cca.luminance_weighted_gray_world(from_pil(image))) #,subblock width(default:20), subblock height(default:20)
      else:
        logger.info("Using no color correction")

      if camera_flips[frame]:
        image = image.rotate(180)
      image.save(PICTURE_FOLDER + '/image%02d_%d.jpg' % (frame, partial_capture_times[frame]), 'jpeg', quality=JPEG_QUALITY)
      image.thumbnail(preview_size, Image.ANTIALIAS)
      image.save(THUMB_FOLDER + '/image%02d_%d_thumb.jpg' % (frame, partial_capture_times[frame]), "jpeg")
    except Exception as e:
      logger.error("ERROR 7")
      logger.debug(e)
      error = "Storing photos failed"
      page = 5
    stream = None
  
  if load_reviews():
    page = 18
  else:
    error = "Storing photos failed"
    page = 5

  loading = False
  preview = False

def take_photos():
  global preview, loading, stream_list, capture_times, page, review_camera, error, fps, last_frame
  preview = False

  if not loading:
    loading = True
    t = threading.Thread(target=show_loader, args=((116,109),True,'white',))
    t.start()
  
  delete_all_jpeg()

  last_camera = current_camera
  stream_list = [io.BytesIO() for _ in range(frames)]
  capture_times = []

  set_camera_values(True)

  camera.resolution = picture_size
  camera.framerate = PICTURE_FRAMERATE
  camera.vflip = VFLIP
  camera.hflip = HFLIP

  if color == 0 or color == 1:
    camera.shutter_speed = camera.exposure_speed
    camera.exposure_mode = 'off'
    g = camera.awb_gains
    camera.awb_mode = 'off'
    camera.awb_gains = g

  start = time.time()

  try:
    camera.capture_sequence(image_stream_generator(), format='jpeg', use_video_port=use_video_port, quality=JPEG_QUALITY)
  except Exception as e:
    error = "Capture sequence error"
    page = 5
    logger.error("ERROR 3")
    logger.debug(e)
    return False

  finish = time.time()
  fps = frames / (finish - start)
  logger.info('Captured %d frames at total %.2ffps' % (frames, fps))

  t_process = threading.Thread(target=process_photos_stream)
  t_process.start()

  time.sleep(0.1)

  select_camera(last_camera)
  btn_cam_select.setBg(icons, 'switcher%d' % current_camera)
  camera.resolution = preview_size
  camera.framerate = preview_framerate

  if color == 0 or color == 1:
    camera.shutter_speed = shutter_us
    camera.exposure_mode = 'auto'
    camera.awb_mode = 'auto'

  review_camera = last_camera
  
  last_frame = False
  preview = False

  page = 20

# UI METHODS
def kill_server():
  logger.debug("going to kill server process")
  try:
    os.killpg(os.getpgid(server.pid), signal.SIGTERM)
    logger.debug("os.killpg success")
  except Exception as e:
    logger.debug("os.kill failed")
    logger.error("ERROR 4")
    logger.debug(e)

def show_loader(pos, show_last_frame, color=False):
  global loading, preview, loader_add_c, loader_position
  loader_position = pos
  logger.debug("Start loader")

  if color:
    loader_add_c = "_%s" % str(color)
  else:
    loader_add_c = ""

  while loading:
    for i in range(5):
      for icon in icons:
        if icon.name == "loader%d%s" % (i,loader_add_c):
          loader_icon = icon
      if loading:
        if last_frame and show_last_frame:
          if preview_size == WIDE_PREVIEW_SIZE: screen.fill((0,0,0))
          screen.blit(last_frame, ((SCREEN_WIDTH - last_frame.get_width()) / 2, (SCREEN_HEIGHT - last_frame.get_height()) / 2))
        screen.blit(loader_icon.bitmap, loader_position)
        pygame.display.update()
        time.sleep(0.15)

  logger.debug("Stopped loader")

def gotopage(new_page):
  global page, loading
  loading = False
  page = new_page

def back_from_preview():
  global review, page, reviews, loading
  logger.debug("Deleting all photos")
  loading = False
  review = False
  reviews = []
  delete_all_jpeg()
  page = 4

def get_preview_frame():
  global last_frame, is_capturing
  is_capturing = True
  if not camera: setup_camera()
  elif not camera._check_camera_open: setup_camera()
  if preview:
    stream = io.BytesIO() # Capture into in-memory stream
    capture_format = 'rgb'
    camera.vflip = camera_flips[current_camera]
    camera.hflip = camera_flips[current_camera]
    camera.capture(stream, use_video_port=True, format=capture_format)
    stream.seek(0)
    stream.readinto(rgb)
    stream.close()
    last_frame = pygame.image.frombuffer(rgb[0:(preview_size[0] * preview_size[1] * 3)], preview_size, 'RGB')
    is_capturing = False
    return last_frame
  is_capturing = False
  return False
def show_review():
  if len(reviews) > 0:
    screen.fill((0, 0, 0))
    screen.blit(reviews[review_camera], ((SCREEN_WIDTH - reviews[review_camera].get_width()) / 2, (SCREEN_HEIGHT - reviews[review_camera].get_height()) / 2))

# PAGE LOADER
def load_page(overwrite_page=None):
  global loading, preview, page, update_screen, gfx_blit, review, allow_shutterbutton, loader_add_c, loader_position

  if overwrite_page != None:
    page = overwrite_page

  bitmap = None
  for pg in pages:
    if pg.name == "page%d" % page:
      bitmap = pg.bitmap
  if not bitmap:
    for pg in pages:
      if pg.name == "bg":
        bitmap = pg.bitmap
    
  screen.blit(bitmap,(0,0))

  allow_shutterbutton = False
  
  if page == 0:
    show_wifi_conf()
    setup_wifi()
  elif page == 1:
    show_wifi_conf()
    t = False
    if not loading:
      loading = True
      t = threading.Thread(target=show_loader, args=((116,204),False,))
      t.start()
    time.sleep(1) # allow wifi reset command to do something
    if internet_on():
      empty_boot_wifi()
      page = 2
    else:
      page = 3
    loading = False
    if t:
      t.join()
  elif page == 4:
    allow_shutterbutton = True
  elif page == 5:
    text = font.render(error, True, (255, 255, 255))
    text_rect = text.get_rect()
    screen.blit(text, (SCREEN_WIDTH / 2 - text_rect.width / 2, 111))
  elif page == 7: # TRYING TO CONNECT
    get_current_ssid()
    t = False
    if not loading:
      loading = True
      t = threading.Thread(target=show_loader, args=((116,143),False,))
      t.start()
    if ssid:
      time.sleep(1)
      if internet_on():
        if not last_frame:
          time.sleep(0.5)
        page = 4
      else:
        page = 3
    else:
      page = 3
    loading = False
    if t:
      t.join()
  elif page == 8: # REBOOT
    t = threading.Thread(target=reboot)
    t.start()
  elif page == 10: # POWEROFF
    t = threading.Thread(target=shutdown)
    t.start()
  elif page == 11: # SHUTTER SETTINGS
    change_setting(11, shutter_range, shutter, 'shutter')
  elif page == 12: # ISO SETTINGS
    change_setting(12, iso_range, iso, 'iso')
  elif page == 13: # ISO SETTINGS
    change_setting(13, exposure_compensation_range, exposure_compensation, 'exposure')
  elif page == 14: # BRIGHTNESS SETTINGS
    change_setting(14, brightness_range, brightness, 'brightness')
  elif page == 15: # CONTRAST SETTINGS
    change_setting(15, contrast_range, contrast, 'contrast')
  elif page == 16: # SIZE SETTINGS
    change_setting(16, size_range, size, 'size')
  elif page == 21: # COLOR SETTINGS
    change_setting(21, color_range, color, 'color')
  elif page == 17: # QUALITY SETTINGS
    change_setting(17, quality_range, quality, 'quality')
  elif page == 18: # PREVIEW PHOTOS
    btn_cam_review.setBg(icons, 'switcher%d' % review_camera)
    review = True
  elif page == 19:
    if not loading:
      loading = True
      t = threading.Thread(target=show_loader, args=((116,153),False,))
      t.start()
  elif page == 20:
    loader_add_c = ""
    loader_position = (116,143)

  # Reset gfx_blit if not page 11 (shutter settings)
  if page not in [11, 12, 13, 14, 15, 16, 17, 21]:
    gfx_blit = False

  # Turn on / off preview
  if page in [4, 6, 11, 12, 13, 14, 15, 16, 17, 21]:
    preview = True
  else:
    preview = False

  if page not in [18, 19]:
    review = False
  
  update_screen = True

# CAMERA SETTINGS
def change_setting(page, setting_range, current_value, prefix, direction = False):
  global update_screen, gfx_blit
  new_value = None
  cur_index = setting_range.index(current_value)
  
  if direction:
    if direction == 'plus':
      new_index = cur_index + 1
      if new_index > len(setting_range) -1:
        new_index = len(setting_range) -1
    elif direction == 'min':
      new_index = cur_index - 1
      if new_index < 0:
        new_index = 0
    new_value = setting_range[new_index]
  else:
    new_index = cur_index
  
  if new_value is None:
    new_value = current_value

  if new_index == len(setting_range) - 1:
    buttons[page][1].setBg(icons, 'plus_disabled')
  else:
    buttons[page][1].setBg(icons, 'plus')
  if new_index == 0:
    buttons[page][0].setBg(icons, 'min_disabled')
  else:
    buttons[page][0].setBg(icons, 'min')

  value_string = 'auto' if new_value == 0 else str(new_value)
  for gfx in gfxs:
    if gfx.name == prefix + '_' + value_string:
      gfx_blit = [[gfx.bitmap, (120, 185)]]

  update_screen = True
  return new_value

def set_shutter(direction):
  global shutter, preview_framerate, shutter_us
  shutter = change_setting(11, shutter_range, shutter, 'shutter', direction)
  if shutter == 0:
    shutter_us = 0
    if preview_framerate != INITIAL_PREVIEW_FRAMERATE:
      preview_framerate = INITIAL_PREVIEW_FRAMERATE
      camera.framerate = preview_framerate
  else:
    shutter_us = int(1000000 / shutter)
    if shutter < preview_framerate:
      preview_framerate = SLOW_PREVIEW_FRAMERATE
      camera.framerate = preview_framerate
    elif preview_framerate != INITIAL_PREVIEW_FRAMERATE:
      preview_framerate = INITIAL_PREVIEW_FRAMERATE
      camera.framerate = preview_framerate
  
  set_camera_values()

def set_iso(direction):
  global iso
  iso = change_setting(12, iso_range, iso, 'iso', direction)
  set_camera_values()

def set_exposure_compensation(direction):
  global exposure_compensation
  exposure_compensation = change_setting(13, exposure_compensation_range, exposure_compensation, 'exposure', direction)
  set_camera_values()
def set_brightness(direction):
  global brightness
  brightness = change_setting(14, brightness_range, brightness, 'brightness', direction)
  set_camera_values()
def set_contrast(direction):
  global contrast
  contrast = change_setting(15, contrast_range, contrast, 'contrast', direction)
  set_camera_values()
def set_size(direction):
  global size
  size = change_setting(16, size_range, size, 'size', direction)
  set_camera_values()
def set_color(direction):
  global color
  color = change_setting(21, color_range, color, 'color', direction)
  set_camera_values()
def set_quality(direction):
  global quality
  quality = change_setting(17, quality_range, quality, 'quality', direction)
  set_camera_values()
def set_camera_values(ingore_res=False):
  global use_video_port, picture_size, swap_delay, preview_size, frames, cam_correction, image_correction
  camera.exposure_compensation = exposure_compensation
  camera.iso = iso
  camera.shutter_speed = shutter_us
  camera.brightness = brightness
  camera.contrast = contrast
  if quality == 0:
    use_video_port = True
    cam_correction = -2
    frames = 8
  else:
    use_video_port = False
    cam_correction = 0
    frames = 4
  picture_size = size_values[size_range.index(size)]
  if size == 1:  
    camera.sensor_mode = 1
    if not ingore_res: camera.resolution = WIDE_PREVIEW_SIZE
    preview_size = WIDE_PREVIEW_SIZE
  else:
    camera.sensor_mode = 0
    if preview_size != DEFAULT_PREVIEW_SIZE:
      if not ingore_res: camera.resolution = DEFAULT_PREVIEW_SIZE
      preview_size = DEFAULT_PREVIEW_SIZE
  if size == -1:
    swap_delay = 0.029
  else:
    swap_delay = 0.025
  if color == 0 or color == 2:
    image_correction = default_image_correction
  else:
    image_correction = 0

  logger.debug("image_correction set to %d" % image_correction)
  
  if color == 0 or color == 1:
    logger.debug("White balance to manual")
    g = camera.awb_gains
    camera.awb_mode = 'off'
    camera.awb_gains = g
  else:
    logger.debug("White balance to auto")
    camera.awb_mode = 'auto'

def init_pygame():
  global font, screen

  # SETUP SERVER
  # server = subprocess.Popen("/home/pi/pi-cam-flask-pygame/server.py", shell=True)
  # atexit.register(kill_server)

  # SETUP PYGAME
  setup_touch_screen()
  pygame.init()
  atexit.register(pygame.quit)
  pygame.font.init()
  font = pygame.font.Font(None, 25)
  pygame.mouse.set_visible(False)
  screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN)

  # SETUP CAMERA
  setup_gpio()
  setup_camera()

  # ATEXIT EXTRAS
  atexit.register(close_screen)

  # SCREEN TIMEOUT
  signal.alarm(SCREEN_TIMEOUT)


# SETUP BUTTONS
def setup_buttons():
  global btn_cam_select, btn_cam_review, buttons
  ## SPECIAL BUTTONS
  btn_cam_select = ui.Button((212, 0, 108, 60), bg='switcher%d' % current_camera, cb=switch_cameras)
  btn_cam_review = ui.Button((92, 162, 136, 96), bg='switcher%d' % review_camera, cb=switch_review)
  ## BUTTONS PER PAGE
  buttons = [
    # page 0 wifi setup:
    [ui.Button((104, 190, 113, 50), bg='done', cb=confirm_wifi),
    ui.Button((244, 85, 55, 50), bg='reload', cb=reload_wifi_conf)],
    # page 1 for connecting to wifi:
    [],
    # page 2 for connected:
    [ui.Button((83, 128, 155, 50), bg='continue', cb=gotopage, value=4)],
    # page 3 for connection failed:
    [ui.Button((61, 128, 200, 50), bg='wifi_settings', cb=gotopage, value=0)],
    # page 4 main camera screen:
    [ui.Button((104, 180, 113, 60), bg='shutter', cb=take_photos),
    btn_cam_select,
    ui.Button((0, 0, 68, 60), bg='settings', cb=gotopage, value=6)],
    # page 5 error screen:
    [ui.Button((88, 162, 146, 58), bg='reboot', cb=gotopage, value=8)],
    # page 6 settings:
    [ui.Button((0, 0, 67, 60), bg='exit', cb=gotopage, value=4),
    ui.Button((224, 0, 41, 60), bg='wifi', cb=gotopage, value=0),
    ui.Button((265, 0, 41, 60), bg='power', cb=gotopage, value=9),
    ui.Button((17, 118, 106, 38), bg='shutter_settings', cb=gotopage, value=11),
    ui.Button((123, 118, 63, 38), bg='iso', cb=gotopage, value=12),
    ui.Button((186, 118, 118, 38), bg='exposure', cb=gotopage, value=13),
    ui.Button((39, 80, 130, 38), bg='brightness', cb=gotopage, value=14),
    ui.Button((169, 80, 113, 38), bg='contrast', cb=gotopage, value=15),
    ui.Button((39, 156, 80, 38), bg='color', cb=gotopage, value=21),
    ui.Button((119, 156, 69, 38), bg='size', cb=gotopage, value=16),
    ui.Button((188, 156, 94, 38), bg='quality', cb=gotopage, value=17)],
    # page 7 trying to connect:
    [],
    # page 8 reboot:
    [],
    # page 9 confirm poweroff:
    [ui.Button((49, 120, 95, 50), bg='cancel', cb=gotopage, value=6),
    ui.Button((144, 120, 127, 50), bg='shut_down', cb=gotopage, value=10)],
    # page 10 poweroff:
    [],
    # page 11 shutter:
    [ui.Button((63, 173, 67, 60), bg='min', cb=set_shutter, value='min'),
    ui.Button((190, 173, 67, 60), bg='plus', cb=set_shutter, value='plus'),
    ui.Button((0, 0, 67, 60), bg='back', cb=gotopage, value=6),
    btn_cam_select],
    # page 12 iso:
    [ui.Button((63, 173, 67, 60), bg='min', cb=set_iso, value='min'),
    ui.Button((190, 173, 67, 60), bg='plus', cb=set_iso, value='plus'),
    ui.Button((0, 0, 67, 60), bg='back', cb=gotopage, value=6),
    btn_cam_select],
    # page 13 exposure:
    [ui.Button((63, 173, 67, 60), bg='min', cb=set_exposure_compensation, value='min'),
    ui.Button((190, 173, 67, 60), bg='plus', cb=set_exposure_compensation, value='plus'),
    ui.Button((0, 0, 67, 60), bg='back', cb=gotopage, value=6),
    btn_cam_select],
    # page 14  brightness:
    [ui.Button((63, 173, 67, 60), bg='min', cb=set_brightness, value='min'),
    ui.Button((190, 173, 67, 60), bg='plus', cb=set_brightness, value='plus'),
    ui.Button((0, 0, 67, 60), bg='back', cb=gotopage, value=6),
    btn_cam_select],
    # page 15 contrast:
    [ui.Button((63, 173, 67, 60), bg='min', cb=set_contrast, value='min'),
    ui.Button((190, 173, 67, 60), bg='plus', cb=set_contrast, value='plus'),
    ui.Button((0, 0, 67, 60), bg='back', cb=gotopage, value=6),
    btn_cam_select],
    # page 16 size:
    [ui.Button((63, 173, 67, 60), bg='min', cb=set_size, value='min'),
    ui.Button((190, 173, 67, 60), bg='plus', cb=set_size, value='plus'),
    ui.Button((0, 0, 67, 60), bg='back', cb=gotopage, value=6),
    btn_cam_select],
    # page 17 quality:
    [ui.Button((63, 173, 67, 60), bg='min', cb=set_quality, value='min'),
    ui.Button((190, 173, 67, 60), bg='plus', cb=set_quality, value='plus'),
    ui.Button((0, 0, 67, 60), bg='back', cb=gotopage, value=6),
    btn_cam_select],
    # page 18 review photos:
    [ui.Button((0, 0, 67, 60), bg='back', cb=back_from_preview),
    btn_cam_review],
    # page 19 locked:
    [],
    # page 20 resizing screen:
    [],
    # page 21 color:
    [ui.Button((63, 173, 67, 60), bg='min', cb=set_color, value='min'),
    ui.Button((190, 173, 67, 60), bg='plus', cb=set_color, value='plus'),
    ui.Button((0, 0, 67, 60), bg='back', cb=gotopage, value=6),
    btn_cam_select]
  ]

def init_gfx_buttons():
  # Load all icons at startup.
  for file in os.listdir(ICON_FOLDER):
    if fnmatch.fnmatch(file, '*.png'):
      icons.append(ui.Gfx(ICON_FOLDER, file.split('.')[0], 'png'))

  # Load all pages at startup.
  for file in os.listdir(PAGE_FOLDER):
    if fnmatch.fnmatch(file, '*.jpg'):
      pages.append(ui.Gfx(PAGE_FOLDER, file.split('.')[0], 'jpg'))

  # Load all graphics at startup.
  for file in os.listdir(GFX_FOLDER):
    if fnmatch.fnmatch(file, '*.png'):
      gfxs.append(ui.Gfx(GFX_FOLDER, file.split('.')[0], 'png'))

  # Assign Icons to Buttons, now that they're loaded
  for button_page in buttons:         # For each screenful of buttons...
    for button in button_page:        # For each button on screen...
      for icon in icons:              # For each icon...
        if button.bg == icon.name:    # Compare names; match?
          button.iconBg = icon        # Assign Icon to Button
          button.bg     = None        # Name no longer used; allow garbage collection
        if button.fg == icon.name:
          button.iconFg = icon
          button.fg     = None

# Handle events map to buttons
def event_handler():
  global loop_time, allow_shutterbutton

  if gp.input(POWER_BUTTON) != gpio_powerbutton_value:
    logger.debug("POWER BUTTON PRESSED")
    close_screen()

  if gp.input(SHUTTER_BUTTON) != gpio_shutterbutton_value:
    logger.debug("SHUTTER_BUTTON BUTTON PRESSED")
    if allow_shutterbutton:
      take_photos()
      allow_shutterbutton = False
      

  for event in pygame.event.get():
    if event.type is MOUSEBUTTONDOWN or event.type is MOUSEBUTTONUP:
      if not backlight:
        if event.type is MOUSEBUTTONUP:
          open_screen()
      else:
        signal.alarm(SCREEN_TIMEOUT)
        loop_time = FAST_LOOP
        pos = pygame.mouse.get_pos()
        for button in buttons[page]:
          if button.selected(screen, event.type, pos): break
    if event.type == KEYDOWN:
      if event.key == K_ESCAPE:
        raise SystemExit

# MAIN LOOP WHERE PYGAME RUNS
def main():
  global prev_page, page, preview, is_capturing, update_screen, error, pygame_running
  capture_timeout_counter = 0


  logger.debug("main()")
  if not pygame_running:
    pygame_running = True
    init_pygame()
    setup_buttons()
    init_gfx_buttons()

    logger.debug("starting while loop in main thread")
    while run_main:
      event_handler()

      if prev_page != page:
        prev_page = page
        load_page()

      if preview and not is_capturing:
        try:
          img = get_preview_frame()
          if not loading:
            if preview_size == WIDE_PREVIEW_SIZE:
              screen.fill((0,0,0))
            if img:
              screen.blit(img, ((SCREEN_WIDTH - img.get_width()) / 2, (SCREEN_HEIGHT - img.get_height()) / 2))
              update_screen = True
        except Exception as e:
          is_capturing = False
          capture_timeout_counter += 1
          logger.error('preview error %d' % capture_timeout_counter)
          logger.error("ERROR 5")
          logger.error(e)
          if capture_timeout_counter > 5:
            error = "Camera error, check connections"
            preview = False
            page = 5

      if review and update_screen and not loading:
        show_review()
      
      if not loading:
        for button in buttons[page]:
          button.draw(screen)

      if gfx_blit and not loading:
        for gfx in gfx_blit:
          screen.blit(gfx[0],gfx[1])

      if update_screen:
        pygame.display.update()
        update_screen = False

      time.sleep(loop_time)
    
    logger.debug("after while loop in main thread")
  else:
    logger.debug("pygame_running was True")
  logger.debug("reached end of main thread")

# SETUP STUFF IN MAIN THREAD
setup_logger()
signal.signal(signal.SIGALRM, timeout_handler)
signal.signal(signal.SIGTERM, lambda num, frame: sys.exit(0))

if __name__ == "__main__":
  #app.logger = logger

  logger.info("Starting main thread")
  t = threading.Thread(target=main)
  run_main = True
  t.start()

  logger.info("Starting server")
  app.run(host='0.0.0.0', port=F_PORT, debug=False)

  run_main = False
  t.join()